// Marcel Kampmann FI-A-15
//Aufgabe 4 auf AB Methoden Übung


public class Aufgabe4Volumenberechnung {

	public static void main(String[] args) {
		
		System.out.println("a)Würfelvolumen:");
		System.out.println(Würfel(7));
		
		System.out.println("b)Quadervolumen:");
		System.out.println(Quader(77, 8, 40));
		
		System.out.println("c)Pyramidenvolumen:");
		System.out.println(Pyramide(2, 7));
		
		System.out.println("d)Kugelvolumen:");
		System.out.println(Kugel(9));
	}
	
	//Würfel Berechnung
	public static double Würfel(double a) {
		return a * a * a;
	}
	
	//Quader Berechnung
	public static double Quader(double a, double b, double c) {
		return a * b * c;
	}
	
	//Pyramiden Berechnung
	public static double Pyramide(double a, double h) {
		return a * a * h/3;
	}
	
	//Kugel Berechnung
	public static double Kugel(double r) {
		return 4/3 * Math.pow(r, 3) * Math.PI;
	}
}
