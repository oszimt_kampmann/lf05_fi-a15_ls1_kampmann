// Marcel Kampmann FI-A-15
// Aufgabe 3, 4 und 5 auf AB Programmierübungen Funktionen/Methoden

public class AufgabenParameterRückgabewert {

	public static void main(String[] args) {
		
	  double x = 77;
      double r1 = 15;
      double r2 = 13;
      
      System.out.println("Gesamtwiederstand in der Reihenschaltung: " + reihenschaltung(r1,r2));
      System.out.println("Gesamtwiderstand in der Parallelschaltung: " + parallelschaltung(r1,r2));
      System.out.println("Das Quadrat von x lautet =" + quadriert(x));
	
      
	}
	//Berechnung Reiehenschaltung
	public static double reihenschaltung(double r1, double r2) {
		return r1 + r2; 
		
	}
	//Berechnung Parallelschaltung
	public static double parallelschaltung(double r3,double r4) {
		return (r3 * r4)/(r3 + r4);
		
	}
	//Berechnung Quadrat
	public static double quadriert(double x) {
		return x*x;
	}
	
}