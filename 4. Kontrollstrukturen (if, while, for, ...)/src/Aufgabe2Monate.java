//Marcel Kampmann F-I-A15
//Der Benutzer gibt eine Ziffer zwischen 1 und 12 ein. 
//Ihr Java-Programm ermittelt den entsprechenden Monat.

import java.util.Scanner;

public class Aufgabe2Monate {

	public static Scanner tastatur = new Scanner(System.in);
	
	public static void main(String[] args) {
		
Integer zahl;
		
		System.out.println("Bitte geben Sie eine beliebige Zahl zwischen 1 und 12 ein: ");
		zahl = tastatur.nextInt();
		
		if (zahl == 1) {
			System.out.println("\nJanuar");
		}
		if (zahl == 2) {
			System.out.println("\nFebruar");
		}	
		if (zahl == 3) {
			System.out.println("\nMärz");
		}	
		if (zahl == 4) {
			System.out.println("\nApril");
		}	
		if (zahl == 5) {
			System.out.println("\nMai");
		}	
		if (zahl == 6) {
			System.out.println("\nJuni");
		}	
		if (zahl == 7) {
			System.out.println("\nJuli");
		}	
		if (zahl == 8) {
			System.out.println("\nAugust");
		}	
		if (zahl == 9) {
			System.out.println("\nSeptember");
		}	
		if (zahl == 10) {
			System.out.println("\nOktober");
		}	
		if (zahl == 11) {
			System.out.println("\nNovember");
		}	
		if (zahl == 12) {
			System.out.println("\nDezember");
		}
		else if (zahl >= 13 || zahl <= 0) {
			System.out.println("\nKein gültiger Monat, bitte wählen Sie eine Zahl zwischen 1 und 12!");
		}
	}

}