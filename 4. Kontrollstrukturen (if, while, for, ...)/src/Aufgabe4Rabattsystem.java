//Marcel Kampmann F-I-A15
//Der Hardware-Großhändler führt ein Rabattsystem ein: Liegt der Bestellwert
//zwischen 0 und 100 €, erhält der Kunde einen Rabatt von 10 %. Liegt der 
//Bestellwert höher, aber insgesamt nicht über 500 €, beträgt der Rabatt 15 %, 
//in allen anderen Fällen beträgt der Rabatt 20 %. Nach Eingabe des Bestellwertes 
//soll der ermäßigte Bestellwert (incl. MwSt.) berechnet und ausgegeben werden.



import java.util.Scanner;

public class Aufgabe4Rabattsystem {

    public static void main(String[] args) {

        System.out.println("Wie hoch ist die Rechnungssumme?");

        Scanner in = new Scanner(System.in);
        double nochZuZahlen = in.nextDouble();

        if(nochZuZahlen > 0 && nochZuZahlen < 100) {
            nochZuZahlen = nochZuZahlen - (nochZuZahlen * 0.10);
            System.out.println("Sie haben 10% Rabatt bekommen!");
            System.out.println("Der Betrag ist " + nochZuZahlen + " €");
        }
        else if(nochZuZahlen > 100 && nochZuZahlen < 500) {
            nochZuZahlen = nochZuZahlen - (nochZuZahlen * 0.15);
            System.out.println("Sie haben 15% Rabatt bekommen!");
            System.out.println("Der Betrag beläuft sich auf " + nochZuZahlen + " €");
        }
        else if(nochZuZahlen > 500) {
            nochZuZahlen = nochZuZahlen - (nochZuZahlen * 0.20);
            System.out.println("Sie bekommen 20% Rabatt auf Ihre Bestellung");
            System.out.println("Der Betrag beläuft sich auf " + nochZuZahlen + " €");
        }
    }
}