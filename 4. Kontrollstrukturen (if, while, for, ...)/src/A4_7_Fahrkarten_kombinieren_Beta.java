//Marcel Kampmann FI-A-15\\
//Arbeitsauftrag 4.7

import java.util.Scanner;

public class A4_7_Fahrkarten_kombinieren_Beta {
    public static void main(String[] args) {
    	
    	while (true) {
	        double zuZahlenderBetrag;
	        double rueckgabebetrag;
	
	        zuZahlenderBetrag = fahrkartenbestellungErfassen();
	        rueckgabebetrag = fahrkartenBezahlen(zuZahlenderBetrag);
	        fahrkartenAusgeben();
	        rueckgeldAusgeben(rueckgabebetrag);
        }
    }

    //Methode fahrkartenbestellungErfassen erweitert -> Zusatzaufgabe 4.7
	//Tickets,Anzahl&Preis erfassen
    public static double fahrkartenbestellungErfassen() {
        int ticket;
    	int anzahlTickets;
        double ticketPreis = 0;
        double gesamtPreis = 0;
        Scanner tastatur = new Scanner(System.in);
        
        do {
        	
        	do {
    	        System.out.println("Wählen Sie Ihre gewünschte Fahrkarte für Tarifbereich Berlin ABC aus:");
    	        System.out.printf("%3s\n", "Einzelfahrschein Regeltarif AB [3,00 Euro] (1)");
    	        System.out.printf("%3s\n", "4-Fahrten-Karte Einzelfahrausweis AB [9,40 Euro] (2)");
    	        System.out.printf("%3s\n", "24-Stunden-Karte Regeltarif AB [8,80 Euro] (3)");
    	        System.out.printf("%3s\n\n", "Bezahlen (9)");
    	        
    	        System.out.print("Ihre Wahl: ");
    	        ticket = tastatur.nextInt();
    	        if (ticket < 1 || ticket > 3) {
    	        	if (ticket == 9 & gesamtPreis != 0) {
        	        	break;
        	        }
    	        	System.out.println("Ungültige Eingabe!\n");
    	        }
            } while (ticket < 1 || ticket > 3);
            	
            if (ticket == 1) {
            	ticketPreis = 3.00;
            } else if (ticket == 2) {
            	ticketPreis = 9.40;
            } else if (ticket == 3){
            	ticketPreis = 8.80;
            } else if (ticket == 9) {
            	break;
            }
            
            do {
            	System.out.print("Wählen Sie bitte eine Anzahl von 1 bis 10 für Ihr Ticket aus: ");
                anzahlTickets = tastatur.nextInt();
                
                if (anzahlTickets < 1 || anzahlTickets > 10) {
                	System.out.printf("\nWählen Sie bitte eine Anzahl von 1 bis 10 für Ihr Ticket aus. \nAnzahl der Tickets:.\n\n", anzahlTickets);
                	anzahlTickets = 1;
                } else {
                	break;
                }
            } while (true);
            
            gesamtPreis += ticketPreis * anzahlTickets;
            System.out.printf("\nZwischensumme: %.2f\n\n", gesamtPreis);
            
        } while (true);

        return gesamtPreis;
    }

    //Bezahlung
    public static double fahrkartenBezahlen(double zuZahlenderBetrag) {
        double eingeworfenemuenze;
        double eingezahlterGesamtbetrag = 0.0;
        Scanner tastatur = new Scanner(System.in);

        while (eingezahlterGesamtbetrag < zuZahlenderBetrag) {
            System.out.format("\nNoch zu zahlen: %4.2f Euro %n", (zuZahlenderBetrag - eingezahlterGesamtbetrag));
            System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
            eingeworfenemuenze = tastatur.nextDouble();
            eingezahlterGesamtbetrag += eingeworfenemuenze;
        }
        return eingezahlterGesamtbetrag - zuZahlenderBetrag;
    }

    // Fahrscheinausgabe
    public static void fahrkartenAusgeben() {
        System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < 8; i++) {
            System.out.print("=");
            try {
                Thread.sleep(250);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println("\n\n");
    }

    // Rückgeldberechnung und -Ausgabe
    public static void rueckgeldAusgeben(double rueckgabebetrag) {

        if (rueckgabebetrag > 0.0) {
            System.out.format("Der R¸ckgabebetrag in Hˆhe von %4.2f Euro %n", rueckgabebetrag);
            System.out.println("wird in folgenden Münzen ausgezahlt:");

            while (rueckgabebetrag >= 2.0) {// 2 EURO-Münzen
                System.out.println("2 EURO");
                rueckgabebetrag -= 2.0;
            }
            while (rueckgabebetrag >= 1.0) {// 1 EURO-Münzen
                System.out.println("1 EURO");
                rueckgabebetrag -= 1.0;
            }
            while (rueckgabebetrag >= 0.5) // 50 CENT-Münzen
            {
                System.out.println("50 CENT");
                rueckgabebetrag -= 0.5;
            }
            while (rueckgabebetrag >= 0.2) // 20 CENT-Münzen
            {
                System.out.println("20 CENT");
                rueckgabebetrag -= 0.2;
            }
            while (rueckgabebetrag >= 0.1) // 10 CENT-Münzen
            {
                System.out.println("10 CENT");
                rueckgabebetrag -= 0.1;
            }
            while (rueckgabebetrag >= 0.05)// 5 CENT-Münzen
            {
                System.out.println("5 CENT");
                rueckgabebetrag -= 0.05;

            }
        }


            	System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                        "vor Fahrtantritt entwerten zu lassen!\n"+
                        "Wir wünschen Ihnen eine gute Fahrt.");
    		}
        }
  