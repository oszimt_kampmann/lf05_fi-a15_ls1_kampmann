//Marcel Kampmann F-I-A15
//Erstellen Sie die Konsolenanwendung Noten. Das Programm Noten soll nach der 
//Eingabe einer Ziffer die sprachliche Umschreibung ausgeben (1 = Sehr gut, 
//2 = Gut, 3 = Befriedigend, 4 = Ausreichend, 5 = Mangelhaft, 6 = Ungenügend). 
//Falls eine andere Ziffer eingegeben wird, soll ein entsprechender 
//Fehlerhinweis ausgegeben werden.

import java.util.Scanner;

public class Aufgabe1Noten {

    public static Scanner tastatur = new Scanner(System.in);

    public static void main(String[] args) {

        byte noten;

        System.out.println("Bitte geben Sie Ihre Note zwischen 1 und 6 ein: ");
        noten = tastatur.nextByte();

        if (noten == 1) {
            System.out.println("\nNote 1 = Sehr gut");
        }
        if (noten == 2) {
            System.out.println("\nNote 2 = Gut");
        }
        if (noten == 3) {
            System.out.println("\nNote 3 = Befriedigend");
        }
        if (noten == 4) {
            System.out.println("\nNote 4 = Ausreichend");
        }
        if (noten == 5) {
            System.out.println("\nNote 5 = Mangelhaft");
        }
        if (noten == 6) {
            System.out.println("\nNote 6 = Ungenügend");
        }
        else if (noten >= 7 ||  noten <= 0) {
            System.out.println("\nKeine gültige Note, bitte überprüfen Sie Ihre Eingabe");
            
        }
        
    }
}