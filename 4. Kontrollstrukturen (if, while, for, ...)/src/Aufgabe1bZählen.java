import java.util.Scanner;

//Marcel Kampmann F-I-A15 Aufgabe 1b: Zählen
//Geben Sie in der Konsole die natürlichen Zahlen von 1 
//bis n heraufzählend (bzw. von n bis 1 herunterzählend)
//aus. Ermöglichen Sie es dem Benutzer die Zahl n festzulegen. 
//Nutzen Sie zur Umsetzung eine for-Schleife.

public class Aufgabe1bZählen {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		
		System.out.println("Bitte nenne mir eine beliebige Zahl?");
		int n = scanner.nextInt();
		for (int i = 1; i <= n; n--) {
			System.out.println(n);
		}
		
		scanner.close();
	}
}