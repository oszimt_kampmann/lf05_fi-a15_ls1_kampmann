import java.util.Scanner;

//Marcel Kampmann F-I-A15 Aufgabe 1: Zählen
//Entwickeln Sie ein Programm, welches die ersten 
//hundert Primzahlen auf dem Bildschirm ausgibt.

public class Aufgabe7Primzahlen {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Bis zur welcher Zahl zwischen 1 und 100 sollen Primzahlen aufgelistet werden?");
        int checkRadius = scanner.nextInt();

        for (int i = 2; i < checkRadius; i++) {
            boolean valid = true;

            for (int j = 2; j < i; j++) {
                if (i % j == 0 && valid) {
                    valid = false;
                    break;
                }
            }

            if (valid) System.out.println(i);
        }

        scanner.close();
    }

}