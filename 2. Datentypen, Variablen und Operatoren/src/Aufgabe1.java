import java.util.Scanner; // Import der Klasse Scanner
public class Aufgabe1 
{
	
	public static void main(String[] args) // Hier startet das Programm
	{
		System.out.println("Marcel Kampmann|A2.2");
		System.out.println();
		
		// Neues Scanner-Objekt myScanner wird erstellt
		Scanner myScanner = new Scanner(System.in);
		System.out.print("Bitte geben Sie eine ganze Zahl ein: ");
		
		// Die Variable zahl1 speichert die erste Eingabe
		double zahl1 = myScanner.nextDouble();
		System.out.print("Bitte geben Sie eine zweite ganze Zahl ein: ");
		
		// Die Variable zahl2 speichert die zweite Eingabe
		double zahl2 = myScanner.nextDouble();
		myScanner.close();
		
		/*
		 * Die Addition der Variablen zahl1 und zahl2
		 * wird der Variable ergebnis zugewiesen.
		 */
		double ADergebnis = zahl1 + zahl2;
		System.out.print("\n\n\nErgebnis der Addition lautet: ");
		System.out.print(zahl1 + " + " + zahl2 + " = " + ADergebnis);
		
		// Die Subtraktion der Variablen zahl1 und zahl2
		// wird der Variable ergebnis zugewiesen.
		double SUergebnis = zahl1 - zahl2;
		System.out.print("\n\n\nErgebnis der Subtraktion lautet: ");
		System.out.print(zahl1 + " - " + zahl2 + " = " + SUergebnis);
		
		// Die Multiplikation der Variablen zahl1 und zahl2
		// wird der Variable ergebnis zugewiesen.
		double MUergebnis = zahl1 * zahl2;
		System.out.print("\n\n\nErgebnis der Multiplikation lautet: ");
		System.out.print(zahl1 + " * " + zahl2 + " = " + MUergebnis);
		
		// Die Division der Variablen zahl1 und zahl2
		// wird der Variable ergebnis zugewiesen.
		double DIergebnis = zahl1 / zahl2;
		System.out.print("\n\n\nErgebnis der Division lautet: ");
		System.out.print(zahl1 + " / " + zahl2 + " = " + DIergebnis);
		

	}

}
