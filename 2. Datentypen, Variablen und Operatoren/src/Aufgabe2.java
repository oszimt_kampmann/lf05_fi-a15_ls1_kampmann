import java.util.Scanner;
public class Aufgabe2 
{

	public static void main(String[] args) 
	{
		System.out.println("Marcel Kampmann|A2.2");
		System.out.println();
		System.out.println();
		System.out.println();
		System.out.println("Herzlich Willkommen!");
		System.out.println();
		
		// Neues Scanner-Objekt NamenScanner wird erstellt
		Scanner myScanner = new Scanner(System.in);
		System.out.print("Bitte geben Sie Ihren Namen an: ");
		
		// Die Variable Namen speichert die erste Eingabe
		String Namen = myScanner.next();
		System.out.print("Bitte geben Sie Ihr Alter an: ");
		
		
		// Die Variable Alter speichert die zweite Eingabe
		String Alter = myScanner.next();
		myScanner.close();
		
		//Zusammenführung der Variablen namen und alter
		System.out.print("Ihr Name ist ");
		System.out.print(Namen);
		System.out.print(" und Sie sind ");
		System.out.print(Alter);
		System.out.print(" Jahre alt.");
		System.out.println();
		System.out.println();
		System.out.println("Auf Wiedersehen!");
		
		
	}

}
