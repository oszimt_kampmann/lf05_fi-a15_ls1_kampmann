
public class Temperaturtabelle {

	public static void main(String[] args) {
		System.out.println("Marcel Kampmann");
		System.out.println("Aufgabe 3");
		System.out.println("LS05.1-A1.6");
		System.out.println();
		System.out.println();
		System.out.println();
System.out.printf("%-12s | %10s%n", "Fahrenheit", "Celsius");
        
        for (int i = 0; i < 25; i++) {
            System.out.print("-");
        }
        
        System.out.print("\n");
        System.out.printf("%-12s | %10.2f%n", "-20", -28.8889);
        System.out.printf("%-12s | %10.2f%n", "-10", -23.3333);
        System.out.printf("%-12s | %10.2f%n", "+0", -17.7778);
        System.out.printf("%-12s | %10.2f%n", "+20", -6.6667);
        System.out.printf("%-12s | %10.2f%n", "+30", -1.1111);
//
	}

}
