
public class WeltDerZahlen {

	  public static void main(String[] args) {
	    
	    /*  *********************************************************
	    
	         Zuerst werden die Variablen mit den Werten festgelegt!
	    
	    *********************************************************** */
	    // Im Internet gefunden ?
	    // Die Anzahl der Planeten in unserem Sonnesystem                    
	    int anzahlPlaneten = 8;
	    
	    // Anzahl der Sterne in unserer Milchstraße
	    long    anzahlSterne = 100000000000l;
	    
	    // Wie viele Einwohner hat Berlin?
	    int    bewohnerBerlin = 3645000;
	    
	    // Wie alt bist du?  Wie viele Tage sind das?
	    
	    int   alterTage = 10500;
	    
	    // Wie viel wiegt das schwerste Tier der Welt?
	    // Schreiben Sie das Gewicht in Kilogramm auf!
	    int   gewichtKilogramm =   190000;
	    
	    // Schreiben Sie auf, wie viele km² das größte Land er Erde hat?
	    int   flaecheGroessteLand = 17098242;
	    
	    // Wie groß ist das kleinste Land der Erde?
	    
	    double   flaecheKleinsteLand = 0.44;
	    
	    
	    
	    
	    /*  *********************************************************
	    
	         Programmieren Sie jetzt die Ausgaben der Werte in den Variablen
	    
	    *********************************************************** */
	    
	    System.out.println("Anzahl der Planeten in unserem Sonnensystem: " + anzahlPlaneten);
	    
	    System.out.println("Anzahl der Sterne in der Milchstraße: " + anzahlSterne);
	    
	    System.out.println("Einwohnerzahl Berlin: " + bewohnerBerlin);
	    
	    System.out.println("Mein Alter in Tagen: " + alterTage);
	    
	    System.out.println("schwerstes Tier der Welt: " + gewichtKilogramm + "kg" + " Blauwal");
	    
	    System.out.println("Fläche größtes Land der Welt: " + flaecheGroessteLand + "km²" + " Russland");
	    
	    System.out.println("Fläche kleinstes Land der Welt: " + flaecheKleinsteLand + "km²" + " Vatikanstaat");
	    
	    System.out.println(" *******  Ende des Programms  ******* ");
	    
	  }
	}


