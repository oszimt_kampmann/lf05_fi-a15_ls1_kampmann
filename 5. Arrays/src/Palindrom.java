//Marcel Kampmann F-I-A15
//Einfache Übungen zu Arrays Aufgabe 3 „Palindrom“

import java.util.Scanner;

public class Palindrom {
    public static void main(String[] args)
    {
        Scanner sc=new Scanner(System.in);
        int n = 5;
        String[] array = new String[n];
        System.out.println("Enter the 5 inputs of the array: ");

        for(int i=0; i<n; i++) {
            array[i]=sc.nextLine();
        }
        System.out.println("Array elements are: ");

        for (int i=array.length-1; i>=0; i--) {
            System.out.println(array[i]);
        }
    }
}
