//Marcel Kampmann F-I-A15
//Einfache Übungen zu Arrays Aufgabe 1 „Zahlen“

public class Zahlen {

	public static void main(String[] args) {

		int[] zahlen = new int[] {1, 2, 3, 4, 5, 6, 7, 8, 9, 0};
	      
		for(int i = 0; i < zahlen.length; i++) {
		   System.out.println(i + 1 +". Zahl:\t"+ zahlen[i]);
		   
		}
	}
}

