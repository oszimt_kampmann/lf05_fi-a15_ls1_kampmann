//Marcel Kampmann F-I-A15
//Einfache Übungen zu Arrays Aufgabe 4 „Lotto“

import java.util.Arrays;

public class Lotto {
    public static void main (String[] args) {
        int lenght = 6;
        String[] array = {"3", "7", "12", "18", "37", "42"};
        System.out.println(Arrays.toString(array));

        for (int i = 0; i < array.length; i++) {
            if (array[i] == "12") {
                System.out.println("Die Zahl 12 ist in der Ziehung enthalten.");

            }
        }
        for (int i = 0; i < array.length; i++) {
            if (array[i] != "13") {
                System.out.println("Die Zahl 13 ist nicht in der Ziehung enthalten.");
                break;
            }
        }
    }
}