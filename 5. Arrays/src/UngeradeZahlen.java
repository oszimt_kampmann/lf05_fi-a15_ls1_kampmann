//Marcel Kampmann F-I-A15
//Einfache Übungen zu Arrays Aufgabe 2 „UngeradeZahlen“

public class UngeradeZahlen {

	public static int[] getOddNumers(int[] oldArray)
    {
        int newSize = 0;
        for (int i = 0; i < oldArray.length; i++)
        {
            if (oldArray[i] % 2 == 1)
            {
                newSize++;
            }
        }
        int newArray[] = new int[newSize];
        for (int i = 0, j = 0; j < newSize; i++)
        {
            if (oldArray[i] % 2 == 1)
            {
                newArray[j++] = oldArray[i];
            }
        }
        return newArray;
    }

    public static void main(String[] args)
    {
        int array[] =
        {
            12, 23, 34, 41, 54, 65, 55, 38, 49, 210
        };

        array = getOddNumers(array);
        for (int i = 0; i < array.length; i++)
        {
            System.out.print(array[i] + " ");
        }
    }

}
