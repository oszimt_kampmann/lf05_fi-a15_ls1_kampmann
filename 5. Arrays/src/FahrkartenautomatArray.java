//Marcel Kampmann FI-A-15\\
//Arbeitsauftrag-LS05.1-A5.3: das Fahrkartenarray

import java.util.Scanner;
public class FahrkartenautomatArray {

    public static void main(String[] args) {

        double zuZahlenderBetrag;
        double rueckgabebetrag;

        zuZahlenderBetrag = fahrkartenbestellungErfassen();
        rueckgabebetrag = fahrkartenBezahlen(zuZahlenderBetrag);
        fahrkartenAusgeben();
        rueckgeldAusgeben(rueckgabebetrag);
    }
    
	//Tickets,Anzahl&Preis erfassen
    public static double fahrkartenbestellungErfassen() {
    	
        // Zuweisung des Werts
        int anzahlTickets;
        double ticketPreis;
        Scanner tastatur = new Scanner(System.in);
        int auswahl;
        String[] bezeichnung = new String[]{"Einzelfahrschein Berlin AB", "Einzelfahrschein Berlin BC", "Einzelfahrschein Berlin ABC", "Kurzstrecke", "Tageskarte Berlin AB", "Tageskarte Berlin BC", "Tageskarte Berlin ABC", "Kleingruppen-Tageskarte Berlin AB", "Kleingruppen-Tageskarte Berlin BC", "Kleingruppen-Tageskarte Berlin ABC"};
        double[] preis = new double[]{2.90, 3.30, 3.60, 1.90, 8.60, 9.00, 9.60, 23.50, 24.30, 24.90};


        System.out.println("Fahrkartenbestellung:");
        System.out.println("•••••••••••••••••••••\n");
        System.out.println("Wählen Sie ihre Wunschfahrkarte für den Tarifbereich Berlin ABC aus:");
        
        for (int i = 0; i < bezeichnung.length; i++) {
            System.out.println(bezeichnung[i] + " - Preis: " + preis[i] + " Fahrkartennummer: " + i);
        }

            System.out.print("Ihre Wahl:");
            auswahl = tastatur.nextInt();

        while (auswahl < 0 || auswahl > 9) {
            System.out.print(" >>falsche Eingabe<<\n " + "Ihre Wahl:");
            auswahl = tastatur.nextInt();
        }

        System.out.print("Anzahl der Tickets: ");
        anzahlTickets = tastatur.nextInt();

        //Zusatzaufgabe 4.6 gelöst
        while (anzahlTickets < 1 || anzahlTickets >= 11) {
            System.out.print("Wählen Sie bitte eine Anzahl von 1 bis 10 für Ihr Ticket aus.\n " + "Anzahl der Tickets: ");
            anzahlTickets = tastatur.nextInt();
        }
        ticketPreis = preis[auswahl];

        return ticketPreis * anzahlTickets;
    }
    
    //Bezahlung
    public static double fahrkartenBezahlen(double zuZahlenderBetrag) {
        double eingeworfenemuenze;
        double eingezahlterGesamtbetrag = 0.0;
        Scanner tastatur = new Scanner(System.in);

        while (eingezahlterGesamtbetrag < zuZahlenderBetrag) {
            System.out.format("Noch zu zahlen: %4.2f € %n", (zuZahlenderBetrag - eingezahlterGesamtbetrag));
            System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
            eingeworfenemuenze = tastatur.nextDouble();
            eingezahlterGesamtbetrag += eingeworfenemuenze;
        }
        return eingezahlterGesamtbetrag - zuZahlenderBetrag;
    }
    
    // Fahrscheinausgabe
    public static void fahrkartenAusgeben() {
        System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < 8; i++) {
            System.out.print("»»»»»");
            try {
                Thread.sleep(450);
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        System.out.println("\n\n");
    }
    
    // Rückgeldberechnung und -Ausgabe
    public static void rueckgeldAusgeben(double rueckgabebetrag) {

        if (rueckgabebetrag > 0.0) {
            System.out.format("Der Rückgabebetrag in Höhe von %4.2f € %n", rueckgabebetrag);
            System.out.println("wird in folgenden Münzen ausgezahlt:");

            while (rueckgabebetrag >= 2.0) {// 2 EURO-Münzen
                System.out.println("2 EURO");
                rueckgabebetrag -= 2.0;
            }
            while (rueckgabebetrag >= 1.0) {// 1 EURO-Münzen
                System.out.println("1 EURO");
                rueckgabebetrag -= 1.0;
            }
            while (rueckgabebetrag >= 0.5) // 50 CENT-Münzen
            {
                System.out.println("50 CENT");
                rueckgabebetrag -= 0.5;
            }
            while (rueckgabebetrag >= 0.2) // 20 CENT-Münzen
            {
                System.out.println("20 CENT");
                rueckgabebetrag -= 0.2;
            }
            while (rueckgabebetrag >= 0.1) // 10 CENT-Münzen
            {
                System.out.println("10 CENT");
                rueckgabebetrag -= 0.1;
            }
            while (rueckgabebetrag >= 0.05)// 5 CENT-Münzen
            {
                System.out.println("5 CENT");
                rueckgabebetrag -= 0.05;
            }
        }
    
        	System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                   "vor Fahrtantritt entwerten zu lassen!\n"+
                   "Wir wünschen Ihnen eine gute Fahrt.");
    }
}

//Schreiben Sie das Programm Fahrkartenautomat (die Methode fahrkartenbestellungErfassen) um:
//Benutzen Sie für die Verwaltung der Fahrkartenbezeichnung und der Fahrkartenpreise jeweils ein Array.
//Welche Vorteile hat man durch diesen Schritt?

//- Arrays sind eine bequemere Möglichkeit, Daten desselben Datentyps mit derselben Größe zu speichern.
//- Schnelle Zugriffszeit auf Teile und Positionen des Codes.
//- Gute Wahl, wenn vorher die Anzahl der Elemente bekannt ist.

//Die Erstellung der Menüeinträge zur Auswahl der Fahrkarten soll mit Hilfe des Arrays aus 1. realisiert werden.
//Es soll möglich sein, nur durch das Verändern der beiden Arrays später neue Tickets oder andere Ticketpreise zu ergänzen.
//Vergleichen Sie die neue Implementierung mit der alten und erläutern Sie die Vor- und Nachteile der jeweiligen Implementierung.

//Vorteile:
//- Einfach gesagt, wird die Übersicht des gesamten Codes durch Arrays verbessert.
//- Zudem wird es einem Außenstehenden leichter gemacht den Code nachzuvollziehen.
//- Speziell in diesem Fall haben wir es durch Arrays deutlich einfacher z.B. neue Preise für 
//  Tickets hinzuzufügen ohne dabei neue komplizierte Schleifen einzufügen.

//Nachteile:
//- Will ein Coder bspw. ein Element an einer Stelle einfügen, müssen alle Elemente in den darauffolgenden Zellen verschoben werden. 
//- Sobald das Array deklariert ist, können wir dessen Größe nicht mehr ändern.
//- Einfügen und Löschen von Datensätzen aus dem Array gestaltet sich als schwierig.